

packer {
  required_plugins {
    digitalocean = {
      version = ">= 1.0.0"
      source  = "github.com/hashicorp/digitalocean"
    }
  }
}







variable "api_token" {
  type    = string
  default = "${env("TF_VAR_do_api_key")}"

}

variable "region" {
  type    = string
  default = "nyc3"
}

variable "drop_size" {
  type    = string
  default = "s-2vcpu-2gb"
}

variable "sn_name" {
  type = string
}

variable "skc_image" {
  type = string
}



source "digitalocean" "skc_build" {
  api_token     = "${var.api_token}"
  image         = "${var.skc_image}"
  region        = "${var.region}"
  size          = "${var.drop_size}"
  snapshot_name = "${var.sn_name}"
  ssh_username  = "root"
  tags          = ["skc_base_ubuntu"]
}


build {
  sources = ["source.digitalocean.skc_build"]

  provisioner "ansible" {
    playbook_file = "../ansible/build.yml"
  }

}

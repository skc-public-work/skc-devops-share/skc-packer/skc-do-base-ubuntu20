
<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [skc-do-base-ubuntu20](#skc-do-base-ubuntu20)
- [useful notes](#useful-notes)
    - [pass variables](#pass-variables)
- [packer commands](#packer-commands)

<!-- markdown-toc end -->




# skc-do-base-ubuntu20


- [ ] packer build of a base Ubuntu 20.04 LTS system pushed to DigitalOcean


# useful notes
## pass variables
- [ ] pass variables

https://www.packer.io/guides/hcl/variables


# packer commands

- [ ] validate
```
packer validate .
```

- [ ] build
```
packer build .
```
